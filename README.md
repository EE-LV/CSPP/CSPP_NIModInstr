CS++ NIModInstr README
======================
This package contains actors and classes derived from CSPP_DeviceBase. Refer to the README.md of [**CSPP_DeviceBase**](https://git.gsi.de/EE-LV/CSPP/CSPP_DeviceBase) for more details.

Develoment based on LabVIEW 2020 SP1.

Required CS++ packages
----------------------

1. [**CSPP_Core**](https://git.gsi.de/EE-LV/CSPP/CSPP_Core): The core package is needed for all CS++ applications.
2. [**CSPP_DeviceBase**](https://git.gsi.de/EE-LV/CSPP/CSPP_DeviceBase): This package contains the bese classes for frequently used (modular) instruments.

CSPP_NIFGen
-----------
This is the actor class implementation using the [NI-Fgen driver](https://www.ni.com/de-de/support/downloads/drivers/download.ni-fgen.html#346233). 

CSPP_NIScope
------------
This is the actor class implementation using the [NI-Scope driver](https://www.ni.com/de-de/support/downloads/drivers/download.ni-scope.html#382068). 

Related information
---------------------------------
Author: H.Brand@gsi.de, D.Neidherr@gsi.de

Copyright 2021  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.